# Ingrid J. Suárez

Eficiencia, trabajo en equipo, rápido aprendizaje, automotivación.

### OBJETIVO

Sacar ventaja de mi experiencia y disciplina en un trabajo que valore mis habilidades, las cuales deseo aplicar para agregar un valor significativo en una empresa dinámica y así garantizar el crecimiento y éxito mutuos.

### APTITUDES

 + Pensamiento analítico, lógico y crítico.
 + Fuerte habilidades interpersonales y de comunicación.
 + Toma de decisiones orientada a las soluciones.
 + Multitasking.
 + Administrativo, organizacional.
 + Investigación avanzada y uso de computadoras y MS Office.
 + Bilingüe

### EXPERIENCIA

##### Guld — *Asistente Personal *

_Enero 2018 - Sept 2018_

Actuar como primer punto de
contacto para tratar
correspondencia y llamadas
telefónicas. Administrar agendas,
organizar reuniones y citas,
controlar el acceso al Director
general y realizar la logística
de viajes, transporte y
alojamiento.

### Pranama — *Asistent e de Gerencia (Pasantía)*

_Noviembre 2017 - Diciembre 2017_

Asistir al gerente en la organización, planificación e implementación de estrategias, evaluar el desempeño de los empleados e identificar las necesidades de contratación y capacitación. Comunicarse con los Comprensión básica de francés y clientes y evaluar su nivel de portugués. satisfacción con la marca y el producto.

### Smart BPO — *Agente de Ventas de Seguros*

_Julio 2017 - Noviembre 2017_

Explicar varias pólizas de seguro y ayudar a los clientes a elegir los planes que les convengan. Seguimiento de pistas anteriores.

### TVN — *Asistente de Presentador*

_Julio 2014 - Julio 2015_

Mantener el programa fluyendo, entrevistar invitados especiales, interactuar con el público, leer guiones frente a cámara, manejo de micrófonos y apuntador. Gestión de redes sociales.

### EDUCACIÓN

##### Colegio Bilingüe Alfred B. Nobel

_Noviembre 2014_

**Bachiller en Ciencias**

##### Copa Airlines — *Entrenamiento para Tripulante de Cabina*

_September 2016 - December 2016_

Psicología del cliente, servicio al cliente, primeros auxilios.

<div class="languages">

### IDIOMAS

| Idioma | Nivel |
|--------|--------|
| Inglés | fluido |
| Español | nativo |
| Francés | básica |
| Portugués | básica |

</div>

<div class="contact">

### Contact

Calle 71, San Francisco

Panama, Panama

**(507) 6835-1546**

**ingridjased@gmail.com**

</div>

